# Formularios Reactivos

Los Formularios Reactivos nos proveen de una manera de manejar las entradas de datos del usuario cuyos valores cambian en el tiempo.

Cada cambio que ocurre en el formulario devuelve un nuevo estado, lo que ayuda a mantener la integridad del modelo entre cada cambio. Los formularios reactivos están basados en flujos de datos de tipo Observable, donde cada entrada y cada valor toman la forma de un flujo de datos que puede ser accedido de manera asíncrona.

## Form Builder
Entra en acción el FormBuilder, un servicio del que han de depender los componentes que quieran desacoplar el modelo de la vista. Se usa para construir un formulario creando un FormGroup, (un grupo de controles) que realiza un seguimiento del valor y estado de cambio y validez de los datos.

![](./imagenes/Selección_157.png)

## Form Group
El formulario se define como un grupo de controles. Cada control tendrá un nombre y una configuración. Esa definición permite establecer un valor inicial al control y asignarle validaciones. En este paso tenemos a disposición varias sobrecargas para configurar con mayor o menor detalle el objeto de control.

## Default data
Para empezar es fácil asignar valores por defecto. Incluso es un buen momento para modificar o transformar datos previos para ajustarlos a cómo los verá el usuario, sin necesidad de cambiar los datos de base.

![](./imagenes/Selección_158.png)

## Enlace en la vista
Mientras tanto en la vista html… Este trabajo previo y extra que se tiente que hacer en el controlador se recompensa con una mayor limpieza en la vista. Lo único necesario será asignar por nombre el elemento html con el control typescript que lo gestionará.

![](./imagenes/Selección_159.png)

## Validación de formularios
La validación es una pieza clave de la entrada de datos en cualquier aplicación. Es el primer frente de defensa ante errores de usuarios; involuntarios o deliberados.

Dichas validaciones se solían realizar agregando atributos html tales como el conocido required. Pero todo eso ahora se traslada a la configuración de cada control, donde podrás establecer una o varias reglas de validación sin mancharte con html.

![](./imagenes/Selección_160.png)

A estas validaciones integradas se puede añadir otras creadas por el programador. Incluso con ejecución asíncrona para validaciones realizadas en el servidor.

## Estados
Los formularios y controles reactivos están gestionados por máquinas de estados que determinan en todo momento la situación de cada control y del formulario en sí mismo.

* VALID: el control ha pasado todos los chequeos
* INVALID: el control ha fallado al menos en una regla.
* PENDING: el control está en medio de un proceso de validación
* DISABLED: el control está desactivado y exento de validación
Cuando un control incumple con alguna regla de validación, estas se reflejan en su propiedad errors que será un objeto con una propiedad por cada regla insatisfecha y un valor o mensaje de ayuda guardado en dicha propiedad.

## Estados de modificación
Los controles, y el formulario, se someten a otra máquina de estados que monitoriza el valor del control y sus cambios.

* PRINSTINE: el valor del control no ha sido cambiado por el usuario
* DIRTY: el usuario ha modificado el valor del control.
* TOUCHED: el usuario ha tocado el control lanzando un evento blur al salir.
* UNTOUCHED: el usuario no ha tocado y salido del control lanzando ningún evento blur.
Como en el caso de los estados de validación, el formulario también se somete a estos estados en función de cómo estén sus controles.

## Valor
Este sistema de gestión de los controles del formulario oculta la parte más valiosa (el valor que se pretende almacenar) en la propiedad value del formulario. Contendrá un objeto con las mismas propiedades usadas durante la definición del formulario, cada una con el valor actual del control asociado.

Un ejemplo típico suele ser como la siguiente vista y su controlador:

![](./imagenes/Selección_161.png)

Ya tenemos formularios reactivos conducidos por los datos que te permitirán construir pantallas complejas manteniendo el control en el modelo y dejando la vista despejada. Como resumen podemos decir que vamos a programar más en TypeScript que en Html. La ventaja del desacople es que podremos controlar lo que enviamos y recibimos de la vista. Así se pueden aplicar formatos, validaciones y transformaciones entre lo que presentamos y lo que enviamos hacia los servicios.

![](./imagenes/Selección_162.png)

<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>
